# Ionic application for searching the nearest events in your city.
Download it in the Play store:
[![alt tag](http://i.imgur.com/I8ehiZ1.png)](https://play.google.com/store/apps/details?id=com.geotap.pequeagenda&hl=en)

# Change Log
All notable changes to this project will be documented in this file.

## [Released]
### Version 1.0.0 Added
- Map added with markers
- Search bar
- Near of you 
- Loading when geoposition and load screen

## [Released]
### Version 1.0.1
- Repair geoposition in Android Devices
- Style search results
- Improve load screen
- Button near of you repair

## [Released]
### Version 1.0.2
- Upload in Apple Store
- Splash screen and icon change
- Share app via whatsapp
- Bug fixes

## [Unreleased]
### Version 1.0.3
- Improvement of loading speed
- Push notifications
- Re-design of event details
