angular.module('starter.services', []).service('EventService', function($http,$q) {

   this.getRecentEvents = function() {
    var deferred = $q.defer();
    var url = "https://www.peque-agenda.com/wp-json/wp/v2/events-api?callback=JSON_CALLBACK&per_page=100";
    $http.get(url)
    .success(function(data) {
      deferred.resolve(data);
      })
    .error(function(data) {
      deferred.reject(data);
      });
    return deferred.promise;
    }
})