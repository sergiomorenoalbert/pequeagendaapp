// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.services','starter.factories','starter.config','ngCordova','firebase'])

.run(function($ionicPlatform,$cordovaPushV5,WpPushServer,EventService) {
  $ionicPlatform.ready(function() {
    // for form inputs)


    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
       var options = {
       android: {
         senderID: "276380860412"
       },
       ios: {
         alert: "true",
         badge: "true",
         sound: "true"
       },
       windows: {}
    };

     // initialize
     $cordovaPushV5.initialize(options).then(function() {
       // start listening for new notifications
       $cordovaPushV5.onNotification();
       // start listening for errors
       $cordovaPushV5.onError();

       // relatedgister to get registrationId
       $cordovaPushV5.register().then(function(data) {
         // `data.registrationId` save it somewhere;

         //console.log("Registration OK????????: " + data);
           // `data.registrationId` save it somewhere;
           if(ionic.Platform.isIOS()){
             //console.log("Registration OK: " + data);
             WpPushServer.storeDeviceToken("ios", data);
           } else if(ionic.Platform.isAndroid())
           {
             console.log("Registration OK ANDROID: " + data);
             WpPushServer.storeDeviceToken("android", data);
             console.log("result: " + data);
           }


       })
     });
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.mapa', {
    url: '/mapa',
    views: { 
      'tab-mapa': {
          templateUrl: 'templates/tab-mapa.html',
          controller: 'MapController'
      }
    }
  })

  .state('tab.cercadeti', {
      url: '/cercadeti',
      views: {
        'tab-cercadeti': {
          templateUrl: 'templates/tab-cercadeti.html',
          controller: 'CercaController'
        }
      }
    })

  .state('tab.hoy', {
    url: '/hoy',
    views: {
      'tab-hoy': {
        templateUrl: 'templates/tab-hoy.html',
        controller: 'HoyController'
      }
    }
  })
  .state('tab.item', {
    url: '/item/:itemId',
    cache:false,
    views: {
      'tab-hoy': {
        templateUrl: 'templates/tab-evento.html',
        controller: 'EventoController'
      }
    }
  })
  .state('tab.item2', {
    url: '/item2/:itemId',
    cache:false,
    views: {
      'tab-cercadeti': {
        templateUrl: 'templates/tab-evento.html',
        controller: 'EventoController'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/mapa');

});
