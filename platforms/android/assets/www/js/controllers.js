var myApp = angular.module('starter.controllers', [])

myApp.controller('MapController', function($scope,$rootScope, EventService, $ionicScrollDelegate,$ionicLoading,$cordovaGeolocation,$cordovaSocialSharing,$timeout) {
    $scope.loading = $ionicLoading.show({
        template:'<div class="animated bounceIn omg" style="width:200px;height:150px;background-image:url(img/mars.gif);background-size:100%;border-radius:10%;background-repeat:no-repeat;"> <h4 class="title-loaders"> Cargando... </h4> </div>',
        animation: 'fade-in',
        showBackdrop: true,
        duration: 8000,
        maxWidth: 200
    });
    $scope.events = [];
    $scope.showDeleteSearchInput=false;
    $scope.maxResults = 5;
    $scope.markers = [];
    var myLatlng = new google.maps.LatLng(40.07493, -3.081388);
    var marker, i;
    var markers = [];
    $scope.markers = markers;
    $scope.marker = marker;

    var mapOptions = {
            center: myLatlng,
            zoom: 5,
            zoomControl: true,
            disableDefaultUI: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_TOP,
            },
            scaleControl: true,
            mapTypeId: 'roadmap',
        };

    $scope.map  = new google.maps.Map(document.getElementById("map"), mapOptions);

    //Get Recent Events from main BD.
    $rootScope.events = [];
    EventService.getRecentEvents()
        .then(function(data) {
            $rootScope.events = data;
            $scope.events = data;
            $scope.centerInitialUser();
            $scope.fillMarkers();
    }); 

    $scope.centerInitialUser = function () {
        var myLocationMarkerContent = "¡Estás aqui! Busca eventos en tu zona y pasalo bien";
        $scope.myLocationMarkerContent = myLocationMarkerContent;
        var myLocationInfoWindow = new google.maps.InfoWindow;
        $scope.myLocationInfoWindow = myLocationInfoWindow;

       var options = {
          enableHighAccuracy: true,
          maximumAge: 10000
       }

       var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

       function onSuccess(position) {
            var lat = position.coords.latitude;
            var long = position.coords.longitude;
            var userPosition = new google.maps.LatLng(lat,long);

            //Marker for current position
            var myLocationMarker = new google.maps.Marker({
                position: userPosition,
                map: $scope.map,
                animation: google.maps.Animation.DROP
            });
            //InfoWindow & marker click
            //Set center and zoom
                        $ionicLoading.hide();           

            $scope.map.setCenter(userPosition);
            $scope.map.setZoom(12);
            $timeout(function() {
                document.getElementById("main-container").classList.remove('ng-hide');
                document.getElementById("main-container").classList.remove('slideOutDown');
                document.getElementById("botInfW").innerHTML =
                    '<figure style="margin-left:10px;">' + myLocationMarkerContent + '</figure>' + '<hr>';
            }, 2500);
       };

       function onError(error) {
          alert("No ha sido posible encontrar tu ubicación, active el acceso a la ubicación");
       }
    };

    $scope.fillMarkers = function() {
        for (i = 0; i < $scope.events.length; i++) {
            var pasesOutput = JSON.stringify($scope.events[i]["acf"]["fecha_pases"]);
            $scope.pasesOutput = pasesOutput;
            if (typeof $scope.events[i]["acf"]["correo_electronico"] === 'undefined' || !$scope.events[i]["acf"]["correo_electronico"]) {
                $scope.events[i]["acf"]["correo_electronico"] = "No disponible...";
            };
            if (typeof $scope.events[i]["acf"]["pagina_web"] === 'undefined' || !$scope.events[i]["acf"]["pagina_web"]) {
                $scope.events[i]["acf"]["pagina_web"] = "No esta disponible...";
            };
            if (typeof $scope.events[i]["acf"]["telefono"] === 'undefined' || !$scope.events[i]["acf"]["telefono"]) {
                $scope.events[i]["acf"]["telefono"] = "No disponible...";
            };

            marker = new google.maps.Marker({
                position: new google.maps.LatLng($scope.events[i]["acf"]["coord"]["lat"], $scope.events[i]["acf"]["coord"]["lng"]),
                map: $scope.map,
                icon: $scope.events[i]["acf"]["imagen_evento"],
                optimized: false
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    $scope.map.setZoom(15);
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                    setTimeout(function() {
                        marker.setAnimation(null);
                    }, 5000);
                    document.getElementById("botInfW").innerHTML =
                        //parte Principal
                        '<figure>' +
                        '<figcaption class="bottomTitleEvents">' + $scope.events[i]["title"]["rendered"] + '</figcaption>' +
                        '<img class="bottomImgEvents" src="' + $scope.events[i]["thumbnail_url"][0] + '" />' +
                        '<figcaption class="bottomContentEvents">' +
                        $scope.events[i]["content"]["rendered"] +
                        //Parte secundaria
                        '<b> Dirección:</b>' + ' ' +
                        $scope.events[i]["acf"]["coord"]["address"] + '<br>' +
                        '<b> Fecha inicio: </b>' + ' ' +
                        $scope.events[i]["acf"]["fecha_inicio"] + '<br>' +
                        '<b> Fecha fin: </b>' + ' ' +
                        $scope.events[i]["acf"]["fecha_fin"] + '<br>' +
                        '<b> Telefono:</b>' + ' ' +
                        $scope.events[i]["acf"]["telefono"] + '<br>' +
                        '<b> Correo electronico:</b>' + ' ' +
                        $scope.events[i]["acf"]["correo_electronico"] + '<br>' +
                        '<b> Página web:</b>' + ' ' +
                        '<a href="' + $scope.events[i]["acf"]["pagina_web"] + '">' + $scope.events[i]["acf"]["pagina_web"] + '</a>' + '<br>' +
                        '<b> Precio:</b>' + ' ' +
                        $scope.events[i]["acf"]["precio"] + '<br>' +
                        '<b> Edad recomendada:</b>' + ' ' +
                        'De ' + $scope.events[i]["acf"]["edad"]["min"] + ' años, a ' + $scope.events[i]["acf"]["edad"]["max"] + ' años. ' + '<br>' + '</div>' +
                        '</figcaption>' +
                        '</figure>' +
                        '<hr>';
                        $scope.map.setCenter(marker.getPosition());
                        document.getElementById("main-container").classList.remove('ng-hide');
                        document.getElementById("main-bounce").classList.remove('ng-hide');
                        document.getElementById("main-container").classList.remove('slideOutDown');
                    }
            }) (marker, i));
                $scope.markers.push(marker);
        };
    };

    $scope.centerOnMe = function() {
        var myLocationMarkerContent = "¡Estás aqui! Busca eventos en tu zona";
        $scope.myLocationMarkerContent = myLocationMarkerContent;
        var myLocationInfoWindow = new google.maps.InfoWindow;
        $scope.myLocationInfoWindow = myLocationInfoWindow;

        $scope.loading2 = $ionicLoading.show({
            template:'<div class="animated bounceIn omg" style="width:200px;height:150px;background-image:url(img/mars.gif);background-size:100%;border-radius:10%;background-repeat:no-repeat;"> <h4 class="title-loaders"> Localizando </h4> </div>',
            animation: 'fade-in',
            showBackdrop: true,
            duration: 3000,
            maxWidth: 200,
            showDelay: 500
        });

        var options = {
          enableHighAccuracy: true,
          maximumAge: 3600000
       }

       var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

       function onSuccess(position) {
            var lat = position.coords.latitude
            var long = position.coords.longitude
            var userPosition = new google.maps.LatLng(lat,long)

            //Marker for current position
            var myLocationMarker = new google.maps.Marker({
                position: userPosition,
                map: $scope.map,
                animation: google.maps.Animation.DROP
            });

            //InfoWindow & marker click
            google.maps.event.addListener(myLocationMarker, 'click', (function(myLocationMarker) {
                return function() {
                    document.getElementById("main-container").classList.remove('ng-hide');
                    document.getElementById("main-container").classList.remove('slideOutDown');
                }
            })(myLocationMarker));
            myLocationMarker.setMap(null);
            //Set center and zoom
            $scope.map.setCenter(userPosition);
            $scope.map.setZoom(17);
            document.getElementById("main-container").classList.remove('ng-hide');
            document.getElementById("main-container").classList.remove('slideOutDown');
            document.getElementById("botInfW").innerHTML =
                '<figure>' + myLocationMarkerContent + '</figure>' +
                '<hr>';
       };

       function onError(error) {
          alert("No ha sido posible encontrar tu ubicación, intentelo más tarde");
       }
    };

    $scope.closeInfo = function() {
            $ionicScrollDelegate.scrollTop(true);
            document.getElementById("main-container").classList.add('slideOutDown');
    };

    $scope.deleteSearch = function(){
        $scope.showDeleteSearchInput=false;
        $scope.showSearchResults=false;
        $scope.searchString = "";
    };

    $scope.onSearchChange = function () {
       if ($scope.searchString) {
            if ($scope.searchString.length>0) {
                $scope.showSearchResults=true;
                $scope.showDeleteSearchInput=true;
                var searchInput = document.getElementById('searchString');
                var resultsOutput = document.getElementById('results');
                var value;
                value = searchInput.value.toLowerCase().split(' ');
                var resultados = $scope.buscarEventos(value);
                $scope.displayResults(resultados);
            } else {
                $scope.showSearchResults=false;
                $scope.showDeleteSearchInput=false;
           }

       }
    };

    $scope.buscarEventos = function (cadena) {
      var results = [];
      for (var i = 0; i < $scope.events.length; i++) {
          if ($scope.events[i]["title"]["rendered"].toLowerCase().search(cadena) >= 0) {
              results.push($scope.events[i]);
          }
      }
      return results;
    };

    $scope.displayResults = function (results) {
      var output = "";
      resultsOutput = document.getElementById('results');
      var total = (results.length > $scope.maxResults ? $scope.maxResults : results.length);
      for (var i = 0, len = total; i < len; i++) {
          output += '<li id="result_' + results[i]["id"] + '">' + results[i]["title"]["rendered"] + '<img class="imageUl" src="' + results[i]["acf"]["imagen_evento"] + '" /></li>';
      }
      resultsOutput.innerHTML = output;
      $scope.anyadirListener(results);
    };

    $scope.anyadirListener = function (results) {
        var getMarkers = function(id) {
            for (var i = $scope.events.length - 1; i >= 0; i--) {
                if ($scope.events[i]["id"]==id) {
                    return markers[i];
                    break;
                }
            }
        }
        var total = (results.length > $scope.maxResults ? $scope.maxResults : results.length);
        for (var i = 0, len = total; i < len; i++) {
              (function(i) {
                  var result_id = 'result_' + results[i]["id"];
                  var result_temp = document.getElementById(result_id);
                  result_temp.addEventListener("click", function() {
                        $scope.showDeleteSearchInput=false;
                        $scope.showSearchResults=false;
                        $scope.searchString = "";
                      google.maps.event.trigger(getMarkers(results[i]["id"]), 'click');
                  }, false);
              }(i));
          }
    };
});
myApp.controller('CercaController', function($scope, $rootScope, $cordovaGeolocation,$ionicLoading,$cordovaSocialSharing) {
        $scope.cargar = $ionicLoading.show({
            template:'<div class="animated bounceIn omg" style="width:200px;height:150px;background-image:url(img/mars.gif);background-size:100%;border-radius:10%;background-repeat:no-repeat;"> <h4 class="title-loaders"> Cargando... </h4> </div>',
            showBackdrop: true,
            duration: 1000
        });
        var options = {
          enableHighAccuracy: true,
          maximumAge: 3600000
        }

       var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

       function onSuccess(position) {
            var lat = position.coords.latitude;
            var long = position.coords.longitude;
            var userPosition = new google.maps.LatLng(lat,long);

            for (i = 0; i < $scope.events.length; i++) {
                $scope.events[i]["distance"] = distance($scope.events[i]["acf"]["coord"]["lat"], $scope.events[i]["acf"]["coord"]["lng"], lat, long);
            }

            function compare(a, b) {
                if (a.distance < b.distance) {
                    return -1;
                } else return 1;
            }
                     
            $scope.events.sort(compare);


            for (i = 0; i < $scope.events.length; i++) {
                if ($scope.events[i]["thumbnail_url"][0] == undefined) {
                    $scope.foto = "img/no-image.png";
                }else{
                    $scope.foto = $scope.events[i]["thumbnail_url"][0]
                }
                if($scope.events[i]["distance"].toFixed(2) <= 50){
                    document.getElementById("columns").innerHTML +=
                        '<figure>' +
                        '<div id="columns-pictures" style="background: url(' + $scope.events[i]["thumbnail_url"][0] + ')center no-repeat; background-size:cover;"> </div>' +
                        '<figcaption>' + $scope.events[i]["title"]["rendered"] + '</figcaption>' +
                        '<figcaption>' +
                        $scope.events[i]["content"]["rendered"].substr(0,$scope.events[i]["content"]["rendered"].indexOf('.')) + "..."+
                        '</figcaption>' +
                        '<hr>'+
                        '<figcaption>' +
                        $scope.events[i]["distance"].toFixed(2) + " Kilometros" +
                        '</figcaption>' +
                        '<hr>'+
                        '<a href="#/tab/item2/'+$scope.events[i]["id"]+'">'+
                        '<figcaption>'+
                        'Más información'+
                        '<span class="ion-ios-arrow-right"> </span>'+
                        '</figcaption> </a>'+
                        '</figure>';
                    }   
                }   
            };
        function distance(lat1, lon1, lat2, lon2) {
            var theta = lon1 - lon2;
            var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            var miles = dist * 60 * 1.1515 * 1.609344;

            return miles;
        }

        function deg2rad(angle) {
            return angle * 0.017453292519943295 // (angle / 180) * Math.PI;
        }

        function rad2deg(dist) {
            return dist / 0.017453292519943295 // (angle / 180) * Math.PI;
        }

       function onError(error) {
          alert("No ha sido posible encontrar tu ubicación, active el acceso a la ubicación");
       }
});
myApp.controller('HoyController', function($scope,$rootScope,$cordovaGeolocation,$ionicLoading,$filter,$cordovaSocialSharing,$state) {
        $scope.cargar = $ionicLoading.show({
            template:'<div class="animated bounceIn omg" style="width:200px;height:150px;background-image:url(img/mars.gif);background-size:100%;border-radius:10%;background-repeat:no-repeat;"> <h4 class="title-loaders"> Cargando... </h4> </div>',
            showBackdrop: true,
            duration: 3000
        });

        var dateObj = $filter('date')(new Date() , "dd/MM/yyyy");;
        actualDate = String(dateObj);
        var temp_pase = [];
        $scope.hoy = [];


        var options = {
          enableHighAccuracy: true,
          maximumAge: 3600000
        }

       var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

       function onSuccess(position) {
            var lat = position.coords.latitude;
            var long = position.coords.longitude;
            var userPosition = new google.maps.LatLng(lat,long);

            for (i = 0; i < $scope.events.length; i++) {
                $scope.events[i]["distance"] = distance($scope.events[i]["acf"]["coord"]["lat"], $scope.events[i]["acf"]["coord"]["lng"], lat, long);
            }

            function compare(a, b) {
                if (a.distance < b.distance) {
                    return -1;
                } else return 1;
            }
            var mySelect = 35;
            $scope.events.sort(compare);
            $scope.showSelectValue = function(mySelect) {
                $ionicLoading.show({
                duration: 1000
                });
                $scope.fillMarkers(mySelect);
            }
            $scope.fillMarkers = function(mySelect) {
                $scope.distanciaEventos = mySelect;
                var output = "";
                $scope.contador = 0;
                var resultsOutput = document.getElementById('eventosDisponibles');
                for (i = 0; i < $scope.events.length; i++) {
                    if ($scope.events[i]["acf"]["fecha_pases"]) {
                        num_pases = $scope.events[i]["acf"]["fecha_pases"].length;
                        for (var j = 0; j < num_pases; j++) {
                            temp_pase = $scope.events[i]["acf"]["fecha_pases"][j]["pase"].substr(0,$scope.events[i]["acf"]["fecha_pases"][j]["pase"].indexOf(' '));
                        }
                        if(temp_pase == actualDate || temp_pase>=actualDate){
                            if(temp_pase != null){
                                //$scope.hoy.push($scope.events[i]);
                                if ($scope.events[i]["thumbnail_url"][0] == undefined) {
                                    $scope.foto = "img/no-image.png";
                                }else{
                                    $scope.foto = $scope.events[i]["thumbnail_url"][0];
                                }
                                if($scope.events[i]["distance"] <= $scope.distanciaEventos) {
                                    $scope.contador++;
                                    output +=
                                    '<figure>' +
                                    '<div id="columns-pictures" style="background: url(' + $scope.foto + ')center no-repeat; background-size:cover;"> </div>' +
                                    '<figcaption style="background-color: #f9f9f9;border-radius: 4px;padding: 10px;"> '+
                                    '<b>'+$scope.events[i]["title"]["rendered"]+'</b>  '+
                                    '</figcaption> '+
                                    '<figcaption style="background-color: #f9f9f9;border-radius: 4px;padding: 10px;"> '+
                                    $scope.events[i]["content"]["rendered"].substr(0,$scope.events[i]["content"]["rendered"].indexOf('.')) +
                                    '</figcaption> '+
                                    '<hr>'+
                                    '<figcaption> <span class="ion-location"> </span> '+
                                    $scope.events[i]["acf"]["coord"]["address"] +
                                    '</figcaption> '+
                                    '<hr>'+
                                    '<figcaption>'+
                                    '<span class="ion-flag"> </span>'+
                                    "A "+$scope.events[i]["distance"].toFixed(2) + " kilometros de ti" +
                                    '<hr>'+
                                    '</figcaption>'+
                                    '<a href="#/tab/item/'+$scope.events[i]["id"]+'">'+
                                    '<figcaption>'+
                                    'Más información'+
                                    '<span class="ion-ios-arrow-right"> </span>'+
                                    '</figcaption> </a>'+
                                    '</figure>';
                                } 
                            }   
                        }
                    }
                }
                resultsOutput.innerHTML = output;
            };
            $scope.fillMarkers(mySelect);

        };
    

        function distance(lat1, lon1, lat2, lon2) {
            var theta = lon1 - lon2;
            var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            var miles = dist * 60 * 1.1515 * 1.609344;

            return miles;
        }

        function deg2rad(angle) {
            return angle * 0.017453292519943295 // (angle / 180) * Math.PI;
        }

        function rad2deg(dist) {
            return dist / 0.017453292519943295 // (angle / 180) * Math.PI;
        }

       function onError(error) {
          alert("No ha sido posible encontrar tu ubicación, active el acceso a la ubicación");
       }
});
myApp.controller('EventoController', function($scope,$rootScope,$stateParams,$ionicLoading) {
        var id = $stateParams.itemId;
        var output = "";
        var resultsOutput = document.getElementById("eventoDetalle");

        for (var i = 0; i < $rootScope.events.length; i++) {
            if(id == $rootScope.events[i]["id"]){
                if ($rootScope.events[i]["thumbnail_url"][0] == undefined) {
                    $scope.foto = "img/no-image.png";
                }else{
                    $scope.foto = $rootScope.events[i]["thumbnail_url"][0]
                }
                output += 
                    '<figure>' +
                    //'<button style="position:fixed;left:20px;margin-top:5px;" class="button button-energized">'+"Mapa"+'</button>'+
                    '<div ng-show="showDown" id="columns-pictures" style="background: url(' + $scope.foto + ')center no-repeat; background-size:cover;"> </div>' +
                    '<figcaption ZoomControlStylee="background-color: #f9f9f9;border-radius: 4px;padding: 10px;">' +
                    '<b>'+$rootScope.events[i]["title"]["rendered"]+'</b>'+  
                    '</figcaption>' +
                    '<figcaption style="background-color: #f9f9f9;border-radius: 4px;padding: 10px;">' +
                    $rootScope.events[i]["content"]["rendered"] +
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption> <span class="ion-location"> </span>' +
                    $rootScope.events[i]["acf"]["coord"]["address"] +
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption>'+
                    '<span> Fecha inicio: </span>' +
                    $rootScope.events[i]["acf"]["fecha_inicio"] +
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption>'+
                    '<span> Fecha fin: </span>' +
                    $rootScope.events[i]["acf"]["fecha_fin"] +
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption>'+
                    '<span class="ion-ios-telephone"> </span>' +
                    '<a href="tel:'+$rootScope.events[i]["acf"]["telefono"]+'">'+$rootScope.events[i]["acf"]["telefono"]+'</a>'+
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption>'+
                    '<span class="ion-ios-email"> </span>' +
                    $rootScope.events[i]["acf"]["correo_electronico"] +
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption>'+
                    '<span class="ion-information-circled"> </span>' +
                    '<a href='+$rootScope.events[i]["acf"]["pagina_web"]+'>' + $rootScope.events[i]["acf"]["pagina_web"] + '</a>' + '<br>' +
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption>'+
                    '<span class="ion-card"> </span>' +
                    $rootScope.events[i]["acf"]["precio"] +
                    '</figcaption>' +
                    '<hr>'+
                    '<figcaption>'+
                    '<span> Edad: </span>' +
                    'De ' + $rootScope.events[i]["acf"]["edad"]["min"] + ' años, a ' + $rootScope.events[i]["acf"]["edad"]["max"] + ' años. ' +
                    '</figcaption>' +
                    '<hr>'+
                    '</figure>';
                //var compiled = $compile(output)($scope);

                var myLatlng = new google.maps.LatLng($rootScope.events[i]["acf"]["coord"]["lat"], $rootScope.events[i]["acf"]["coord"]["lng"]);
                var mapOptions = {
                  center: myLatlng,
                  zoom: 16,
                  disableDefaultUI: true,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map1"),
                    mapOptions);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    icon: $rootScope.events[i]["acf"]["imagen_evento"],
                    optimized: false
                });
                $scope.map = map;
            }

            $scope.ShowDown = false;
            $scope.ShowUp = true;

            $scope.showHide = function () {
                $scope.ShowDown = $scope.ShowDown ? false : true;
                $scope.ShowUp = $scope.ShowUp ? false : true;
            }
    
            resultsOutput.innerHTML = output;
        }
});
